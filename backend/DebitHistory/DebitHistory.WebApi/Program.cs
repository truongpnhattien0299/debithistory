// using DebitHistory.Infrastructure.Identity.Models;
// using Microsoft.AspNetCore.Hosting;
// using Microsoft.AspNetCore.Identity;
// using Microsoft.Extensions.Configuration;
// using Microsoft.Extensions.DependencyInjection;
// using Microsoft.Extensions.Hosting;
// using Microsoft.Extensions.Logging;
// using System;
// using System.Threading.Tasks;

// namespace DebitHistory.WebApi
// {
//     public class Program
//     {
//         public async static Task Main(string[] args)
//         {
//             //Read Configuration from appSettings
//             var config = new ConfigurationBuilder()
//                 .AddJsonFile("appsettings.json")
//                 .Build();

//             //Initialize Logger
//             var host = CreateHostBuilder(args).Build();
//             using (var scope = host.Services.CreateScope())
//             {
//                 var services = scope.ServiceProvider;
//                 var loggerFactory = services.GetRequiredService<ILoggerFactory>();
//                 try
//                 {
//                     var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
//                     var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

//                     await Infrastructure.Identity.Seeds.DefaultRoles.SeedAsync(userManager, roleManager);
//                     await Infrastructure.Identity.Seeds.DefaultSuperAdmin.SeedAsync(userManager, roleManager);
//                     await Infrastructure.Identity.Seeds.DefaultBasicUser.SeedAsync(userManager, roleManager);
//                 }
//                 catch (Exception ex)
//                 {
//                 }
//                 finally
//                 {
//                 }
//             }
//             host.Run();
//         }
//         public static IHostBuilder CreateHostBuilder(string[] args) =>
//             Host.CreateDefaultBuilder(args)
//                 .ConfigureWebHostDefaults(webBuilder =>
//                 {
//                     webBuilder.UseStartup<Startup>();
//                 });
//     }
// }
using DebitHistory.Application.Interfaces;
using DebitHistory.Application;
using DebitHistory.Infrastructure.Identity;
using DebitHistory.WebApi.Extensions;
using DebitHistory.WebApi.Services;
using DebitHistory.Infrastructure.Persistence;
using DebitHistory.Infrastructure.Shared;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);
var _config = builder.Configuration;
var services = builder.Services;

services.AddApplicationLayer();
services.AddIdentityInfrastructure(_config);
services.AddPersistenceInfrastructure(_config);
services.AddSharedInfrastructure(_config);
services.AddSwaggerExtension();
services.AddControllers();
services.AddApiVersioningExtension();
services.AddHealthChecks();
services.AddScoped<IAuthenticatedUserService, AuthenticatedUserService>();

// App
var app = builder.Build();
app.UseHttpsRedirection();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.UseSwaggerExtension();
app.UseErrorHandlingMiddleware();
app.UseHealthChecks("/health");
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}"
);

// using (var scope = host.Services.CreateScope())
//             {
//                 var services = scope.ServiceProvider;
//                 var loggerFactory = services.GetRequiredService<ILoggerFactory>();
//                 try
//                 {
//                     var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
//                     var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

//                     await Infrastructure.Identity.Seeds.DefaultRoles.SeedAsync(userManager, roleManager);
//                     await Infrastructure.Identity.Seeds.DefaultSuperAdmin.SeedAsync(userManager, roleManager);
//                     await Infrastructure.Identity.Seeds.DefaultBasicUser.SeedAsync(userManager, roleManager);
//                 }
//                 catch (Exception ex)
//                 {
//                 }
//                 finally
//                 {
//                 }
//             }

app.Run();
