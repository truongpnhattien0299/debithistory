﻿using AutoMapper;
using DebitHistory.Application.Features.Products.Commands.CreateProduct;
using DebitHistory.Application.Features.Products.Queries.GetAllProducts;
using DebitHistory.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DebitHistory.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<Product, GetAllProductsViewModel>().ReverseMap();
            CreateMap<CreateProductCommand, Product>();
            CreateMap<GetAllProductsQuery, GetAllProductsParameter>();
        }
    }
}
